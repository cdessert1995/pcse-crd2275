#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int fib(int n,int *done, int *sequence, omp_lock_t donelock, omp_lock_t fiblock){ 
  int i, j;  
  //Only do something if you haven't already done it
  if(done[n] == 0){
	//Base case 
    if (n<2){
      return n;
    }
	//Other cases
    else{
      //Compute previous case with locks
      omp_set_lock(&donelock);
      omp_set_lock(&fiblock);          
      #pragma omp task shared(i) firstprivate(n)
      {
      i=fib(n-1,done,sequence,donelock,fiblock);
      }
      omp_unset_lock(&donelock);
      omp_unset_lock(&fiblock);

	  //Compute next case with locks
      omp_set_lock(&donelock);
      omp_set_lock(&fiblock);
      #pragma omp task shared(j) firstprivate(n)
      {
      j=fib(n-2,done,sequence,donelock,fiblock);
      }
      omp_unset_lock(&donelock);
      omp_unset_lock(&fiblock);

	  //Finish all tasks before adding
      #pragma omp taskwait
      sequence[n] = i+j;
	  //Set done to true
      done[n] = 1;
    }
  }
  return sequence[n];
}

//Declare global variables
int done[10];
int sequence[10];

int main(int argc, char **argv){
  //Initialize values to variables
  int n = 10;
  int i;
  for(i=0;i<n;i++){
    done[i]=0;
    sequence[i]=0;
  } 
  omp_lock_t donelock;
  omp_lock_t fiblock; 

  //Start locking
  omp_init_lock(&donelock);
  omp_init_lock(&fiblock);

  //Begin OMP
  omp_set_dynamic(0);
  omp_set_num_threads(4);
  #pragma omp parallel shared(n,done)
  {
    #pragma omp single
    printf ("The %dth number of the Fibonacci sequence is %d.\n", n, fib(n,done,sequence,donelock,fiblock));
  }

  omp_destroy_lock(&donelock);
  omp_destroy_lock(&fiblock);
}
