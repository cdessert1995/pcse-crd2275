#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char *argv[]){
	int rank;
	int i;
	MPI_Init( &argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	for(i = (1413*rank) + 2; i < (1413*(rank + 1)) + 2; i++){
		if(2000000111%i == 0){
    		printf("The factors of 2000000111 are %d and %d\n", i, 2000000111/i);
    	}
	}	
  	MPI_Finalize();
  	return 0;
}
