#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int N = 100;

struct path{
  int number;
  char dir[400][6];
};

struct path traverse(int array[N][N], int row, int column, struct path traversal,int visited[N][N]){

  visited[row][column] = 1;
  if(row != (N - 1) || column != (N - 1)){


    struct path right;
    struct path down;

    right.number = (N*N) + 1;
    down.number = (N*N) + 1;

 
   #pragma omp task
    {
      if((column != (N - 1)) && (array[row][col + 1] == 1) && (visited[row][col + 1] != 1)){
        right = traversal;
        right.number = traversal.number + 1; 
        strcpy(right.dir[right.number],"Right");
        right =  traverse(array, row, column + 1, right, visited); 
      }
    }
 
    #pragma omp task
    {
      if((row != (N -1 )) && (array[row +1 ][column] == 1) && (visited[row + 1][col] != 1)){
        down = traversal;
        down.number = traversal.number + 1;
        strcpy(down.dir[down.number],"Down");
        down = traverse(array, row + 1,column, down, visited);
      }
    }
    
   #pragma omp taskwait
   #pragma omp critical
    {
      if(down.number <= right.number){
        return down;
      }
      else{
        return right;
      }
  
    }
  }

  if(row == (N - 1) && column == (N - 1)){
    return traversal;
  }
    
}


int main(){
  int i,j;
  int array[N][N];
  int visited[N][N];

  for(i = 0; i < N; i++){
    for(j = 0; j < N; j++){
      visited[i][j] = 0;
    }
  }

  struct path traversal;
  traversal.number=0;
  struct path shortest;

  FILE *f;
  f = fopen("hundred.dat","r");
  for(i = 0; i < N; i++){
    for(j =0; j < N; j++){
      fscanf(f,"%1d",&array[i][j]);     
    }
    fscanf(f,"\n");
  }

  #pragma omp parallel
  {
    #pragma omp single
    {
      shortest = traverse(array, 0, 0, traversal, visited);
    }
  }

  int cur_row = 0;
  int cur_column = 0;

  printf("Steps: %d\n", shortest.number);
  printf("(0,0)\n");
  for(i = 1; i < shortest.number + 1; i++){
    if(strcmp(shortest.dir[i],"Right")== 0){
      cur_column += 1;
    }
    else{
      cur_row += 1;
    }
    printf("%6s (%d,%d)\n", i, shortest.dir[i], cur_row, cur_column);
  }

  return 0;
}
