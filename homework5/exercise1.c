#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

int main(int argc, char *argv[]){
	//Start MPI
	MPI_Init(&argc,&argv);
	int rank;
	int size;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm subcomm_column;
	MPI_Comm subcomm_row;
	double sqrt_size = sqrt((double)size);
	if(sqrt_size != floor(sqrt_size)){
		printf("Number of processes needs to be a perfect square.\n");
		MPI_Finalize();
		return 0;
	}
	int grid_length = (int) sqrt_size;

	//Define Processes
	int row_num = rank % grid_length;
	int column_num = rank / grid_length;

	//Split into communicators
	MPI_Comm_split(MPI_COMM_WORLD,column_num,rank,&subcomm_column);
	MPI_Comm_split(MPI_COMM_WORLD,row_num,rank,&subcomm_row);
  
	//Define Broadcast Values
	int bcast_value[2];
	if(column_num == 0){
    	bcast_value[0] = row_num;
  	} 
  	if(row_num == 0){
    	bcast_value[1] = column_num;
  	}

	//Broadcast
  	MPI_Bcast(&bcast_value[0],1,MPI_INT,0,subcomm_row);
  	MPI_Bcast(&bcast_value[1],1,MPI_INT,0,subcomm_column);  	
	
	//Print out recieved numbers
	printf("Rank: %2d Row: %2d Column: %2d\n", rank, bcast_value[0], bcast_value[1]);

	//End
 	MPI_Finalize();
  	return 0;
}
