#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "mpi.h"

//Define functions
void print_matrix(MPI_Comm communicator2, double rand_var);
void transpose(MPI_Comm communicator, double *rand_var);

int main(int argc, char *argv[]){
  	//Start MPI
	MPI_Init(&argc,&argv);
  	int rank;
	int size;
  	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  	MPI_Comm_size(MPI_COMM_WORLD,&size);
   	double log_size = (double)log(size)/(double)log(2.0);
  	if((log_size / 2.0) != floor(log_size/2.0)){
    	printf("Number of processes must be 4^n for a 2^n by 2^n matrix. Exiting...\n");
    	MPI_Finalize();
    	return 0;
  	} 

	//Generate random numbers
  	double rand_var;
  	srand(time(NULL)+(rank*(double)RAND_MAX/size));
  	rand_var = (rand()/ (double)RAND_MAX);
	
	//Print matrix to be transposed
	if(rank == 0){
    	printf("The matrix to be transposed is\n");
  	}
  	print_matrix(MPI_COMM_WORLD,rand_var);
  	if(rank == 0){
    	printf("\n");
  	}
  
	//Do the transposition
  	transpose(MPI_COMM_WORLD,&rand_var);

	//Print the transposed matrix 
  	print_matrix(MPI_COMM_WORLD,rand_var); 
  	
	//End program
	MPI_Finalize();
   	return 0;
}

void transpose(MPI_Comm communicator,double *rand_var){
	//Declare variables 
 	int rank,size;
  	int color;
  	MPI_Comm_rank(communicator,&rank);
  	MPI_Comm_size(communicator,&size);
  	int row_size = (int) sqrt(size);
  	int half_row_size = row_size / 2;
  	int i,j;
  	
	//Base case does nothing
  	if(size == 1){
    	return;
  	} 
 	
	//Split top left into color 1
  	for(i=0;i<half_row_size;i++){
    	for(j=0;j<half_row_size;j++){
    	  	if(rank == (row_size*i+j)){
        		color = 1;
      		} 
   		}
  	}
  	
	//Split top right into color 2
  	for(i=0;i<half_row_size;i++){
    	for(j=half_row_size;j<row_size;j++){
      		if(rank == (row_size*i+j)){
        		color = 2;
      		}
    	}
  	}	
  
	//Split bottom left into color 3
  	for(i=half_row_size;i<row_size;i++){
    	for(j=0;j<half_row_size;j++){
      		if(rank == (row_size*i+j)){
       		 	color = 3;
    	  	}
    	}
  	}

	//Split bottom right into color 4
  	for(i=half_row_size;i<row_size;i++){
    	for(j=half_row_size;j<row_size;j++){
      		if(rank == (row_size*i+j)){
        		color = 4;
      		}
   		}
  	}
  
	//Split communicator into four communicators
  	MPI_Comm sub_communicator; 
  	MPI_Comm_split(communicator,color,rank,&sub_communicator);
  
	//Define the local set of matrix values
  	double local_array[size*size/4];
  
	//Define the local set of root processes
  	int local_root;
  	if(((rank % row_size) < half_row_size) && (rank < (size/2))){local_root = 0;}
  	else if(((rank % row_size) >= half_row_size) && (rank < (size/2))){local_root = half_row_size;}
  	else if(((rank % row_size) < half_row_size) && (rank >= (size/2))){local_root = size/2;}
  	else {local_root = (size/2) + half_row_size;}

	//Define a temporary double
	double trans_scalar = (*rand_var);
 
	//Gather the local set of matrix values from the temporary values
  	MPI_Gather(&trans_scalar,1,MPI_DOUBLE,&local_array,1,MPI_DOUBLE,0,sub_communicator);

	//Define the recieved array
  	double rec_array[size*size/4];  
  
	//Send and recieve the array  between the local roots colors 2 and 3
	if(rank == half_row_size){
    	MPI_Sendrecv(&local_array,size*size/4,MPI_DOUBLE,size/2,0,&rec_array,size*size/4,MPI_DOUBLE,size/2,77,communicator,MPI_STATUS_IGNORE);
  	}
  	if(rank == size/2){
   	MPI_Sendrecv(&local_array,size*size/4,MPI_DOUBLE,half_row_size,77,&rec_array,size*size/4,MPI_DOUBLE,half_row_size,0,communicator,MPI_STATUS_IGNORE);
  	}

	//Scatter the received array into the temporary values on colors 2 and 3
  	if(((rank % row_size) >= half_row_size) && (rank < (size/2))){ 
    	MPI_Scatter(&rec_array, 1,MPI_DOUBLE,&trans_scalar,1,MPI_DOUBLE,0,sub_communicator);
  	}
  	if(((rank % row_size) < half_row_size) && (rank >= (size/2))){ 
     	MPI_Scatter(&rec_array, 1,MPI_DOUBLE,&trans_scalar,size*size/4,MPI_DOUBLE,0,sub_communicator);  
  	}
	
	//If the values changed, make the changes permanent
  	*rand_var = trans_scalar;

	//Repeat recursively for all colors  
  	transpose(sub_communicator,rand_var);
}

void print_matrix(MPI_Comm communicator2, double rand_var){
	//Define variables	
    int rank,size;
  	MPI_Comm_rank(communicator2,&rank);
  	MPI_Comm_size(communicator2,&size);
  	double data[size];
  	int i,j;
		
	//Gather the array
	MPI_Gather(&rand_var,1,MPI_DOUBLE,data,1,MPI_DOUBLE,0,communicator2);
  
	//Print it out
  	if(rank == 0){
	    for(i = 0; i<sqrt(size); i++){
    		for(j = 0; j<sqrt(size);j++){
        		printf("%f ",data[i*(int)sqrt(size)+j]);
	      	}
    	  	printf("\n");
    	}
  	} 
}

