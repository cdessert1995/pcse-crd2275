#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "mpi.h"

void print_matrix(MPI_Comm commun, double random_fraction);
void transpose(MPI_Comm comm, double *random_fraction);

int main(int argc, char *argv[]){
  MPI_Init(&argc,&argv);

  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  double log_size = (double)log(size)/(double)log(2.0);
  if((log_size / 2.0) != floor(log_size/2.0)){
    printf("Number of processes needs to be 2^(2n) for some n!\n");
    MPI_Finalize();
    return 0;
  } 

  double random_fraction;
  srand(time(NULL)+(rank*(double)RAND_MAX/size));
  random_fraction = (rand()/ (double)RAND_MAX);
  if(rank == 0){
    printf("Initial matrix:\n");
  }
  print_matrix(MPI_COMM_WORLD,random_fraction);
  if(rank == 0){
    printf("\n\n");
  }
  
  transpose(MPI_COMM_WORLD,&random_fraction); 
  print_matrix(MPI_COMM_WORLD,random_fraction); 
  MPI_Finalize();
  
  return 0;
}

void transpose(MPI_Comm comm,double *random_fraction){
  int rank,size;
  int colour;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);
  int row_size = (int) sqrt(size);
  int half_row_size = row_size / 2;
  int i,j;
  
  if(size == 1){
    return;
  } 
 
  for(i=0;i<half_row_size;i++){
    for(j=0;j<half_row_size;j++){
      if(rank == (row_size*i+j)){
        colour = 1;
      } 
    }
  }
  
  for(i=0;i<half_row_size;i++){
    for(j=half_row_size;j<row_size;j++){
      if(rank == (row_size*i+j)){
        colour = 2;
      }
    }
  }
  
  for(i=half_row_size;i<row_size;i++){
    for(j=0;j<half_row_size;j++){
      if(rank == (row_size*i+j)){
        colour = 3;
      }
    }
  }

  for(i=half_row_size;i<row_size;i++){
    for(j=half_row_size;j<row_size;j++){
      if(rank == (row_size*i+j)){
        colour = 4;
      }
    }
  }
  
  MPI_Comm sub_comm; 
  MPI_Comm_split(comm,colour,rank,&sub_comm);
  
  double local_array[size*size/4];
  
  int local_root;
  if(((rank % row_size) < half_row_size) && (rank < (size/2))){local_root = 0;}
  else if(((rank % row_size) >= half_row_size) && (rank < (size/2))){local_root = half_row_size;}
  else if(((rank % row_size) < half_row_size) && (rank >= (size/2))){local_root = size/2;}
  else {local_root = (size/2) + half_row_size;}

 
  double trans_scalar = (*random_fraction);
 
  MPI_Gather(&trans_scalar,1,MPI_DOUBLE,&local_array,1,MPI_DOUBLE,0,sub_comm);

  double rec_array[size*size/4];  
  


  if(rank == half_row_size){
     MPI_Sendrecv(&local_array,size*size/4,MPI_DOUBLE,size/2,0,&rec_array,size*size/4,MPI_DOUBLE,size/2,77,comm,MPI_STATUS_IGNORE);
  }

  if(rank == size/2){
    MPI_Sendrecv(&local_array,size*size/4,MPI_DOUBLE,half_row_size,77,&rec_array,size*size/4,MPI_DOUBLE,half_row_size,0,comm,MPI_STATUS_IGNORE);
  }


  if(((rank % row_size) >= half_row_size) && (rank < (size/2))){ 
    MPI_Scatter(&rec_array, 1,MPI_DOUBLE,&trans_scalar,1,MPI_DOUBLE,0,sub_comm);
  }

  if(((rank % row_size) < half_row_size) && (rank >= (size/2))){  
    MPI_Scatter(&rec_array, 1,MPI_DOUBLE,&trans_scalar,size*size/4,MPI_DOUBLE,0,sub_comm);  
  }

  *random_fraction = trans_scalar;

  
  transpose(sub_comm,random_fraction);

}

void print_matrix(MPI_Comm commun, double random_fraction){
  
  int rank,size;
  MPI_Comm_rank(commun,&rank);
  MPI_Comm_size(commun,&size);
  double data[size];
  MPI_Gather(&random_fraction,1,MPI_DOUBLE,data,1,MPI_DOUBLE,0,commun);
  int i,j;

  if(rank == 0){
    for(i = 0; i<sqrt(size); i++){
      for(j = 0; j<sqrt(size);j++){
        printf("%f ",data[i*(int)sqrt(size)+j]);
      }
      printf("\n");
    }

  } 
 
 }
