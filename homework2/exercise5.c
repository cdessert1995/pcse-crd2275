#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

int main(int argc, char *argv[]){
	MPI_Init(&argc, &argv);
	int rank;
	int size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	srand((unsigned) rank + time(NULL));
	double random = (rand()/(double)RAND_MAX);
	printf("%f			%d\n", random, rank);
	double randarray[32];
	double max;
	int rankmax;
	int i;
	MPI_Gather(&random, 1, MPI_DOUBLE, randarray, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	if (rank == 0){
		max = randarray[0];
		rankmax = 0;
		for (i = 1; i < 32; i++){
			if (randarray[i] > max){
				max = randarray[i];
				rankmax = i;
			}
		}
		printf("The maximum value is %f on processor %d.", max, rankmax);
	}
	MPI_Finalize();
	return 0;
}
