#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

int main(int argc, char **argv){
	int nsteps = 1000000000;
	double tstart,tend,elapsed,pi,quarterpi,h;
	if (argc != 2){
		printf("Must pass in number of chunks. Exiting...\n");
	}
	int n = atoi(argv[1]);
	int i;
	tstart = omp_get_wtime(); //gettime();
	quarterpi = 0.; 
	h = 1./nsteps;
	int nsamples = 0;

	#pragma omp parallel for schedule(guided, n)
	for (i=0; i<nsteps; i++){
		double x = i*h, x2 = (i+1)*h, y = sqrt(1-x*x), y2 = sqrt(1-x2*x2);
		double slope = (y-y2)/h;
		if (slope>15){
			slope = 15;
		}
		int samples = 1+(int)slope, is;
		for (is=0; is<samples; is++){
			double hs = h/samples, xs = x+ is*hs, ys = sqrt(1-xs*xs);
			quarterpi += hs*ys;
			nsamples++;
		}
	}


 	pi = 4*quarterpi;
 	tend = omp_get_wtime(); //gettime();
 	elapsed = tend-tstart;
 	printf("Computed pi=%e in %6.3f seconds with chunk size %d\n", pi, elapsed, n);
 	return 0;
}
