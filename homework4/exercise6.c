#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]){
	//Declare variables
	int rank;
	int size;
	int i;
	int stage;
	double numi;
	double numf;	
	double initial_t;
	double final_t;
	
	//Start MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	if (size % 2 != 0){
		printf("Must have even number of processors");
		MPI_Finalize();
		return 0;
	}
	
	//Assign numbers
	srand(time(NULL)+(rank*(double)RAND_MAX/size));
	numi = (rand()/(double)RAND_MAX);
	numf = numi;
	
	//Exchange Sort Algorithm
	initial_t = MPI_Wtime();
	for(stage = 0; stage < size/2; stage++){
		//Even Stage
		//Even rank send
		if(rank % 2 == 0){
		MPI_Sendrecv(&numi, 1, MPI_DOUBLE, rank + 1, 0, &numf, 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); 
        //Odd rank send
		}else{
		MPI_Sendrecv(&numi, 1, MPI_DOUBLE, rank - 1, 0, &numf, 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		//If lower-ranked process recieves a lower number, it should store it
		if(rank % 2 == 0){
			if(numi>numf){
				numi = numf;
			}
		//If higher-ranked process recieves a higher number, it should store it
		}else{
			if(numi<numf){
				numi = numf;
			}
		}
		//Odd Stage	
        if((rank % 2 == 1) && (rank != size-1)){
		MPI_Sendrecv(&numi, 1, MPI_DOUBLE, rank + 1, 0, &numf, 1, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		//Odd rank send 
		}else if((rank % 2 == 0) && (rank != 0)){
		MPI_Sendrecv(&numi, 1, MPI_DOUBLE, rank - 1, 0, &numf, 1, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
		//If lower-ranked process recieves a lower number, it should store it
		if((rank % 2 == 1) && (rank != size-1)){
            if(numi>numf){
        		numi = numf;
        }
        //If higher-ranked process recieves a higher number, it should store it
        }else if((rank % 2 == 0) && (rank != 0)){
        	if(numi<numf){
              	numi = numf;
            }
        }
	}
	final_t = MPI_Wtime();
	
	//Output results
	double results[size];
	//printf("Rank: %d, Numi: %f\n", rank, numi);
	MPI_Gather(&numi, 1, MPI_DOUBLE, results, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	if(rank == 0){
		for(i = 0; i < size; i++){
			printf("%f \n", results[i]);
		}
		printf("\n Time elapsed: %lf", final_t-initial_t);
	}		

	//End Program
	MPI_Finalize();
	return 0;
}
