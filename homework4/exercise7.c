#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]){
    //Declare variables
    int rank;
    int size;
    int predecessor;
	int sucessor;
	double variable;
	double variable_pre;
	double variable_suc;
	double avg;
	
	//Start MPI
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

	//Initialize random numbers and assign
	srand(time(NULL)+rank*(double)RAND_MAX/size);
	variable = (rand()/(double)RAND_MAX);
	
	//Assign processors swapping partners
	if (rank != 0){
		predecessor = rank - 1;
	}else{
		predecessor = MPI_PROC_NULL;
	}
	if (rank != size - 1){
		sucessor = rank + 1;
	}else{
		sucessor = MPI_PROC_NULL;
	}
	
	//Send to predessor and sucessors
	MPI_Request pre;
	MPI_Request suc;
	MPI_Isend(&variable, 1, MPI_DOUBLE, predecessor, 0, MPI_COMM_WORLD, &pre);
	MPI_Isend(&rank, 1, MPI_DOUBLE, sucessor, 0, MPI_COMM_WORLD, &suc);
	
	//Recieve from predessor and succesor
	variable_pre = 0.0;
	variable_suc = 0.0;
	MPI_Request Recv[2];
	MPI_Irecv(&variable_pre,1, MPI_DOUBLE, predecessor, 0, MPI_COMM_WORLD, &Recv[0]);
	MPI_Irecv(&variable_suc,1, MPI_DOUBLE, sucessor, 0, MPI_COMM_WORLD, &Recv[1]);
	MPI_Waitall(2, Recv, MPI_STATUSES_IGNORE);

	//Find average
	if ((rank != 0) && (rank != size - 1)){
		avg = (variable + variable_pre + variable_suc)/3.0;
	}else{
		avg = (variable + variable_pre + variable_suc)/2.0;
	}
	
	//Print initial and averaged values
	printf("Rank: %d, Random Value: %lf, Averaged Value: %lf\n", rank, variable, avg);

	//End
	MPI_Finalize();
	return 0;
}
 
