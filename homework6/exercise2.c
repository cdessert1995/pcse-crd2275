#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>

int main(int argc, char *argv[]){
    double start = omp_get_wtime(); 
    if(argc != 3){
        printf("Must pass in number of elements.\n");
    }
    int N = atoi(argv[1]);
    int nthreads = atoi(argv[2]);
    double a[N][N];
    double b[N][N];
    double c[N][N];
    
    srand(time(NULL));
 
    int i,j,k;
    for(i=0;i<N;i++){
      	for(j=0;j<N;j++){
        	a[i][j] = rand()/ (double)RAND_MAX;
       		b[i][j] = rand()/ (double)RAND_MAX;
        	c[i][j] = 0.0;
      	}
    }

    #pragma omp parallel num_threads(nthreads)
	{
    	#pragma omp for
    	for(i=0;i<N;i++){
      		for(j=0;j<N;j++){
        		for(k=0;k<N;k++){
          			c[i][j] += a[i][k] * b[k][j];
        		}
      		}
    	}
	}
    
    double diff = omp_get_wtime() - start;
    printf("%f\n", diff);
    return 0;
}


