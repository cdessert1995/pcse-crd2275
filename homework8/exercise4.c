#include <stdio.h>
#include <stdlib.h>
#include "omp.h"

void print_matrix(int N, double matrix[N][N]);

int main(int argc, char *argv[]){

  if(argc != 2){
    printf("Require size of matrix!\n");
    return -1;
  }

  int N = atoi(argv[1]);
  int i,j,k;

  double A[N][N];
  double B[N][N];
  double C[N][N];

  srand(time(NULL));
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      A[i][j] = rand()/ (double)RAND_MAX;
      B[i][j] = rand()/ (double)RAND_MAX;
      C[i][j] = 0.0;
    }
  }

  if(N<=10){
    printf("Matrix A\n");
    print_matrix(N,A);
    printf("\nMatrix B\n");
    print_matrix(N,B);
  }

  double start_time, end_time;
  start_time = omp_get_wtime();
  double temp=0;

  #pragma omp parallel 
  {
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      #pragma omp for reduction(+:temp) schedule(static)
      for(k=0;k<N;k++){
        temp += A[i][k] * B[k][j];
      }
      C[i][j] = temp;
      temp = 0;
    }
  }
  }  
 
  end_time = omp_get_wtime();

  if(N<=10){
    printf("Multipled Matrix\n");
    print_matrix(N,C);
  }

  printf("\nSize:%d  Time:%3.3f\n", N,end_time-start_time);

  return 0;
}

void print_matrix(int N, double matrix[N][N]){
  int i,j; 
  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      printf("%3.3f ",matrix[i][j]);
    }
    printf("\n");
 }
  
}
