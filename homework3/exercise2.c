#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <math.h>

//Ping-Pong
int main(int argc, char *argv[]){

	//MPI
	MPI_Init(&argc, &argv);
	int rank;
	int size;
	int message_size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	//Ensure correct user input
	if(argc != 2){
		printf("You must give number of trials and message size! Exiting...");
		exit;
	}
	if(message_size > 9){
		printf("Message size cannot exceed 9. Exiting...");
		exit;
	}

	//Declare variables
	const int num_trials = argv[1];
	message_size = argv[2];
	int current_trial;
	int initial_message;
	int final_message;
	int i;
	double initial_t;
	double final_t;
	double message_timing = 0;
	
	//Begin ping-pong
	for(current_trial = 0; current_trial < num_trials; current_trial++){
		//Construct message
		initial_message = 0;
		for(i = 0; i < message_size; i++){
			initial_message = initial_message + (int) pow(10,i)*i;
		}
		
		//Send and receive message
		initial_t = MPI_Wtime();
		if(rank == 0){
			initial_message = final_message;
			MPI_Send(&final_message, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
			printf("Message \"%d\" sent to process 1 by process 0\n", final_message);
		}else if(rank == 1){
			MPI_Recv(&final_message, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			if (initial_message =! final_message){
				printf("Message received is different than what was sent.\n");
			}else{
				printf("Message \"%d\" recieved from process 0 by process 1\n", final_message);
			}
		}
		final_t = MPI_Wtime();
		message_timing += final_t - initial_t;
	}
	
	//Final information
	if(rank == 0){
		printf("It took %lf seconds to complete %d trials.", message_timing, num_trials);
		printf("Average timing was %lf.", message_timing/(double) num_trials);
	}
	
	//End
	MPI_Finalize();
	return 0;
}
