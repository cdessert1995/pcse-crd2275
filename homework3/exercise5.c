#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

//Right Shift
int main(int argc, char *argv[]){
	
	//Set up MPI
	MPI_Init(&argc, &argv);
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	//Define neighbors
	int left_nb;
	int right_nb;
	if(rank != 0){
		left_nb = rank - 1;
	}else{
		left_nb = MPI_PROC_NULL;
	}
	if(rank != 31){
		right_nb = rank + 1;
	}else{
		right_nb = MPI_PROC_NULL;
	}
	
	//Send/Receive
	MPI_Sendrecv(&rank, 1, MPI_INT, right_nb, 0, &rank, 1, MPI_INT, left_nb, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	if(rank != 0){
		printf("Process %d sent %d to process %d\n", left_nb, rank, rank);
	}

	//End
	MPI_Finalize();
	return 0;
} 
